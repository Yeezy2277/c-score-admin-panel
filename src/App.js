import React, {useEffect, useLayoutEffect, useState} from 'react';
import Routes from './routes/index';
import {useLocation, redirect, useNavigate} from 'react-router-dom';
import {Session, SessionContextProvider} from '@supabase/auth-helpers-react';
import {createClient} from '@supabase/supabase-js';
import './assets/scss/themes.scss';

// Fake Backend
import fakeBackend from './helpers/AuthType/fakeBackend';

fakeBackend();

function App() {
    const location = useLocation();
    const [supabaseClient] = useState(() => createClient(process.env.REACT_APP_PUBLIC_SUPABASE_URL, process.env.REACT_APP_PUBLIC_SUPABASE_ANON_KEY)); // Replace with your Supabase URL and API key
    console.log(supabaseClient);
    const routerAsPath = location.pathname;
    const navigate = useNavigate();
    const [session, setSession] = useState(null);
    useLayoutEffect(  () => {
        //http://localhost:3000/#error=unauthorized_client&error_code=401&error_description=Email+link+is+invalid+or+has+expired
        async function getUrl() {
            const urlParams = new URLSearchParams(window.location.hash.substring(1))
            const accessToken = urlParams.get('access_token');
            const refreshToken = urlParams.get('refresh_token');
            if (accessToken && refreshToken) {
                console.log(accessToken, refreshToken);
                const {data, error} = await supabaseClient.auth.setSession({
                    access_token: accessToken,
                    refresh_token: refreshToken,
                })
                console.log(data, error);
                if (!error) {
                    navigate('/');
                }
            }
        };
        getUrl();
    }, []);
    useEffect(async () => {
        const session = await supabaseClient.auth.getSession();
        setSession(session.data.session);
        supabaseClient.auth.onAuthStateChange((event) => {
            if ((
                event === 'SIGNED_OUT' &&
                (routerAsPath === '/' || routerAsPath.startsWith('/dashboard') || routerAsPath.startsWith('/management'))
            ) || !session.data.session && (routerAsPath === '/' || routerAsPath.startsWith('/dashboard') || routerAsPath.startsWith('/management'))) {
                navigate('/signin');
            }
        });
    }, [supabaseClient.auth, routerAsPath]);

    return (
        <SessionContextProvider supabaseClient={supabaseClient} initialSession={session}>
            <React.Fragment>
                <Routes/>
            </React.Fragment>
        </SessionContextProvider>
    );
}

export default App;
