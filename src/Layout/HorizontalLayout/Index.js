import React, { useEffect, useState } from 'react';
import { useNavigate, Outlet } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import {
    changeLayout,
    changeTopbarTheme,
    changeLayoutWidth,
    showRightSidebarAction,
    changelayoutMode
} from "../../store/actions";

import NavBar from "./NavBar";
import Header from "./Header";
import Footer from "./Footer";
import RightSidebar from "../../components/Common/RightSidebar";

const Layout = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const {
        topbarTheme, layoutWidth, breadCrumbItems, showRightSidebar, layoutMode
    } = useSelector(state => ({
        topbarTheme: state.Layout.topbarTheme,
        layoutWidth: state.Layout.layoutWidth,
        isPreloader: state.Layout.isPreloader,
        showRightSidebar: state.Layout.showRightSidebar,
        breadCrumbItems: state.Breadcrumb.breadCrumbItems,
        layoutMode: state.Layout.layoutMode,
    }));

    useEffect(() => {
        const title = navigate().location.pathname;
        let currentage = title.charAt(1).toUpperCase() + title.slice(2);
        document.title = currentage + " | C-SCORE";
    }, []);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    const hideRightbar = (event) => {
        const rightbar = document.getElementById("right-bar");
        if (rightbar && rightbar.contains(event.target)) {
            return;
        } else {
            dispatch(showRightSidebarAction(false));
        }
    };

    useEffect(() => {
        dispatch(changeLayout("horizontal"));
        document.body.addEventListener("click", hideRightbar, true);
    }, [dispatch]);

    useEffect(() => {
        if (layoutMode) {
            dispatch(changelayoutMode(layoutMode));
        }
    }, [layoutMode, dispatch]);

    useEffect(() => {
        if (topbarTheme) {
            dispatch(changeTopbarTheme(topbarTheme));
        }
    }, [topbarTheme, dispatch]);

    useEffect(() => {
        if (layoutWidth) {
            dispatch(changeLayoutWidth(layoutWidth));
        }
    }, [layoutWidth, dispatch]);

    const [isMenuOpened, setIsMenuOpened] = useState(false);
    const openMenu = () => {
        setIsMenuOpened(!isMenuOpened);
    };

    return (
        <React.Fragment>
            <header id="page-topbar" className="ishorizontal-topbar">
                <Header breadCrumbItems={breadCrumbItems} openLeftMenuCallBack={openMenu}/>
                <NavBar menuOpen={isMenuOpened}/>
            </header>
            <div className="main-content">
                <Outlet /> {/* Render nested routes */}
            </div>
            {showRightSidebar ? <RightSidebar /> : null}
            <Footer />
        </React.Fragment>
    );
};

export default Layout;
