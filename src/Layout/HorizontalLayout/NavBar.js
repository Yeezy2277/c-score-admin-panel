import { Link, Outlet } from "react-router-dom";
import { Collapse, Col, Container, Row } from 'reactstrap';
import PropTypes from "prop-types";
import React, { useState, useEffect } from "react";
import classname from "classnames";
import { connect } from 'react-redux';
import { useSupabaseClient } from "@supabase/auth-helpers-react";

const NavBar = (props) => {
    const [dashboard, setdashboard] = useState(false);
    const [ui, setui] = useState(false);
    const [app, setapp] = useState(false);
    const [email, setemail] = useState(false);
    const [ecommerce, setecommerce] = useState(false);
    const [project, setproject] = useState(false);
    const [contact, setcontact] = useState(false);
    const [component, setcomponent] = useState(false);
    const [form, setform] = useState(false);
    const [table, settable] = useState(false);
    const [chart, setchart] = useState(false);
    const [icon, seticon] = useState(false);
    const [map, setmap] = useState(false);
    const [pages, setpages] = useState(false);
    const [invoice, setinvoice] = useState(false);
    const [auth, setauth] = useState(false);
    const [utility, setutility] = useState(false);

    const supabaseClient = useSupabaseClient();

    const logoutFunc = async () => {
        const { error, data } = await supabaseClient.auth.signOut();
        console.log(data, error);
    }

    useEffect(() => {
        var matchingMenuItem = null;
        var ul = document.getElementById("navigation");
        var items = ul.getElementsByTagName("a");
        removeActivation(items);
        for (var i = 0; i < items.length; ++i) {
            if (props.location.pathname === items[i].pathname) {
                matchingMenuItem = items[i];
                break;
            }
        }
        if (matchingMenuItem) {
            activateParentDropdown(matchingMenuItem);
        }
    }, [props.location.pathname]);

    const removeActivation = (items) => {
        for (var i = 0; i < items.length; ++i) {
            var item = items[i];
            const parent = items[i].parentElement;
            if (item && item.classList.contains("active")) {
                item.classList.remove("active");
            }
            if (parent) {
                if (parent.classList.contains("active")) {
                    parent.classList.remove("active");
                }
            }
        }
    };

    const activateParentDropdown = (item) => {
        item.classList.add("active");
        const parent = item.parentElement;
        if (parent) {
            parent.classList.add("active"); // li
            const parent2 = parent.parentElement;
            parent2.classList.add("active"); // li
            const parent3 = parent2.parentElement;
            if (parent3) {
                parent3.classList.add("active"); // li
                const parent4 = parent3.parentElement;
                if (parent4) {
                    parent4.classList.add("active"); // li
                    const parent5 = parent4.parentElement;
                    if (parent5) {
                        parent5.classList.add("active"); // li
                        const parent6 = parent5.parentElement;
                        if (parent6) {
                            parent6.classList.add("active"); // li
                        }
                    }
                }
            }
        }
        return false;
    }

    return (
        <React.Fragment>
            <div className="topnav">
                <Container fluid>
                    <nav className="navbar navbar-light navbar-expand-lg topnav-menu active" id='navigation'>
                        <Collapse isOpen={props.leftMenu} className="navbar-collapse" id="topnav-menu-content">
                            <ul className="navbar-nav">
                                <li className="nav-item dropdown">
                                    <Link
                                        to="/dashboard"
                                        className="nav-link dropdown-toggle arrow-none"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            setdashboard(!dashboard);
                                        }}
                                    >
                                        <i className="bx bx-home-circle me-2"></i>
                                        Dashboard {props.menuOpen}
                                        <div className="arrow-down"></div>
                                    </Link>
                                    <div
                                        className={classname("dropdown-menu", { show: dashboard })}
                                    >
                                        <Link to="/index" className="dropdown-item" data-key="t-ecommerce">
                                            Ecommerce
                                        </Link>
                                        <Link to="/dashboard-saas" className="dropdown-item" data-key="t-saas">
                                            Saas
                                        </Link>
                                        <Link to="/dashboard-crypto" className="dropdown-item" data-key="t-crypto">
                                            Crypto
                                        </Link>
                                    </div>
                                </li>

                                <li className="nav-item dropdown">
                                    <Link
                                        to="/ui"
                                        onClick={(e) => {
                                            e.preventDefault();
                                            setui(!ui);
                                        }}
                                        className="nav-link dropdown-toggle arrow-none"
                                    >
                                        <i className="bx bx-tone me-2"></i>
                                        UI Elements <div className="arrow-down"></div>
                                    </Link>
                                    <div
                                        className={classname(
                                            "dropdown-menu mega-dropdown-menu dropdown-menu-left dropdown-mega-menu-xl",
                                            { show: ui }
                                        )}
                                    >
                                        <Row>
                                            <Col lg={4}>
                                                <div>
                                                    <Link to="/ui-alerts" className="dropdown-item" data-key="t-alerts">
                                                        Alerts
                                                    </Link>
                                                    <Link to="/ui-buttons" className="dropdown-item" data-key="t-buttons">
                                                        Buttons
                                                    </Link>
                                                    <Link to="/ui-cards" className="dropdown-item" data-key="t-cards">
                                                        Cards
                                                    </Link>
                                                    <Link to="/ui-carousel" className="dropdown-item" data-key="t-carousel">
                                                        Carousel
                                                    </Link>
                                                    <Link to="/ui-dropdowns" className="dropdown-item" data-key="t-dropdowns">
                                                        Dropdowns
                                                    </Link>
                                                    <Link to="/ui-grid" className="dropdown-item" data-key="t-grid">
                                                        Grid
                                                    </Link>
                                                    <Link to="/ui-images" className="dropdown-item" data-key="t-images">
                                                        Images
                                                    </Link>
                                                </div>
                                            </Col>
                                            <Col lg={4}>
                                                <div>
                                                    <Link to="/ui-lightbox" className="dropdown-item" data-key="t-lightbox">
                                                        Lightbox
                                                    </Link>
                                                    <Link to="/ui-modals" className="dropdown-item" data-key="t-modals">
                                                        Modals
                                                    </Link>
                                                    <Link to="/ui-offcanvas" className="dropdown-item" data-key="t-offcanvas">
                                                        Offcanvas
                                                    </Link>
                                                    <Link to="/ui-rangeslider" className="dropdown-item" data-key="t-range-slider">
                                                        Range Slider
                                                    </Link>
                                                    <Link to="/ui-progressbars" className="dropdown-item" data-key="t-progress-bars">
                                                        Progress Bars
                                                    </Link>
                                                    <Link
                                                        to="/ui-tabs-accordions"
                                                        className="dropdown-item"
                                                        data-key="t-tabs-accordions"
                                                    >
                                                        Tabs & Accordions
                                                    </Link>
                                                    <Link to="/ui-typography" className="dropdown-item" data-key="t-typography">
                                                        Typography
                                                    </Link>
                                                </div>
                                            </Col>
                                            <Col lg={4}>
                                                <div>
                                                    <Link to="/ui-video" className="dropdown-item" data-key="t-video">
                                                        Video
                                                    </Link>
                                                    <Link to="/ui-general" className="dropdown-item" data-key="t-general">
                                                        General
                                                    </Link>
                                                    <Link to="/ui-colors" className="dropdown-item" data-key="t-colors">
                                                        Colors
                                                    </Link>
                                                    <Link to="/ui-rating" className="dropdown-item" data-key="t-rating">
                                                        Rating
                                                    </Link>
                                                    <Link to="/ui-notifications" className="dropdown-item" data-key="t-notifications">
                                                        Notifications
                                                    </Link>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </li>

                                {/* ... (other menu items) */}

                            </ul>
                        </Collapse>
                    </nav>
                </Container>
            </div>

            <Outlet /> {/* This will render child routes */}
        </React.Fragment>
    );
};

NavBar.propTypes = {
    leftMenu: PropTypes.any,
    location: PropTypes.any,
    menuOpen: PropTypes.any,
    t: PropTypes.any,
};

const mapStatetoProps = state => {
    const { leftMenu } = state.Layout;
    return { leftMenu };
};

export default connect(mapStatetoProps, {})(NavBar);
