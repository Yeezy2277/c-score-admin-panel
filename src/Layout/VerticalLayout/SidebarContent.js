import React, { useCallback, useEffect, useRef } from "react";
import PropTypes from "prop-types";
import { Link, useLocation, Outlet } from "react-router-dom";
import SimpleBar from "simplebar-react";
import MetisMenu from "metismenujs";

const SidebarContent = () => {
    const ref = useRef();
    const location = useLocation();

    const activateParentDropdown = useCallback((item) => {
        item.classList.add("active");
        const parent = item.parentElement;
        const parent2El = parent.childNodes[1];

        if (parent2El && parent2El.id !== "side-menu") {
            parent2El.classList.add("mm-show");
        }

        if (parent) {
            parent.classList.add("mm-active");
            const parent2 = parent.parentElement;

            if (parent2) {
                parent2.classList.add("mm-show");
                const parent3 = parent2.parentElement;

                if (parent3) {
                    parent3.classList.add("mm-active");
                    parent3.childNodes[0].classList.add("mm-active");
                    const parent4 = parent3.parentElement;

                    if (parent4) {
                        parent4.classList.add("mm-show");
                        const parent5 = parent4.parentElement;

                        if (parent5) {
                            parent5.classList.add("mm-show");
                            parent5.childNodes[0].classList.add("mm-active");
                        }
                    }
                }
            }
            scrollElement(item);
            return false;
        }
        scrollElement(item);
        return false;
    }, []);

    const removeActivation = (items) => {
        for (let i = 0; i < items.length; ++i) {
            const item = items[i];
            const parent = item.parentElement;

            if (item && item.classList.contains("active")) {
                item.classList.remove("active");
            }
            if (parent) {
                const parent2El =
                    parent.childNodes && parent.childNodes.length && parent.childNodes[1]
                        ? parent.childNodes[1]
                        : null;
                if (parent2El && parent2El.id !== "side-menu") {
                    parent2El.classList.remove("mm-show");
                }

                parent.classList.remove("mm-active");
                const parent2 = parent.parentElement;

                if (parent2) {
                    parent2.classList.remove("mm-show");

                    const parent3 = parent2.parentElement;
                    if (parent3) {
                        parent3.classList.remove("mm-active");
                        parent3.childNodes[0].classList.remove("mm-active");

                        const parent4 = parent3.parentElement;
                        if (parent4) {
                            parent4.classList.remove("mm-show");
                            const parent5 = parent4.parentElement;
                            if (parent5) {
                                parent5.classList.remove("mm-show");
                                parent5.childNodes[0].classList.remove("mm-active");
                            }
                        }
                    }
                }
            }
        }
    };

    const activeMenu = useCallback(() => {
        const pathName = location.pathname;
        const fullPath = pathName;
        let matchingMenuItem = null;
        const ul = document.getElementById("side-menu");
        const items = ul.getElementsByTagName("a");
        removeActivation(items);

        for (let i = 0; i < items.length; ++i) {
            if (fullPath === items[i].pathname) {
                matchingMenuItem = items[i];
                break;
            }
        }
        if (matchingMenuItem) {
            activateParentDropdown(matchingMenuItem);
        }
    }, [location.pathname, activateParentDropdown]);

    useEffect(() => {
        ref.current.recalculate();
    }, []);

    useEffect(() => {
        new MetisMenu("#side-menu");
        activeMenu();
    }, [activeMenu]);

    useEffect(() => {
        activeMenu();
    }, [activeMenu]);

    function scrollElement(item) {
        if (item) {
            const currentPosition = item.offsetTop;
            if (currentPosition > window.innerHeight) {
                ref.current.getScrollElement().scrollTop = currentPosition - 300;
            }
        }
    }

    return (
        <React.Fragment>
            <SimpleBar className="sidebar-menu-scroll" ref={ref}>
                <div id="sidebar-menu">
                    <ul className="metismenu list-unstyled" id="side-menu">
                        <li className="menu-title" data-key="t-menu">Menu</li>

                        <li>
                            <Link to="/#" className="has-arrow">
                                <i className="icon nav-icon" data-eva="grid-outline"></i>
                                <span className="menu-item" data-key="t-dashboards">Dashboards</span>
                            </Link>
                            <ul className="sub-menu">
                                <li><Link to="/dashboard" data-key="t-ecommerce">Charts and graphs</Link></li>
                                <li><Link to="/dashboard-activity" data-key="t-saas">Affiliates activity analysis</Link></li>
                            </ul>
                        </li>

                        <li className="menu-title" data-key="t-applications"></li>

                        <li>
                            <Link to="/management" className="has-arrow">
                                <i className="icon nav-icon" data-eva="person-done-outline"></i>
                                <span className="menu-item" data-key="t-management">Account Management</span>
                            </Link>
                            <ul className="sub-menu">
                                <li><Link to="/management-info" data-key="t-ecommerce">Account Info</Link></li>
                            </ul>
                        </li>

                        <li className="menu-title" data-key="t-applications"></li>

                        <li>
                            <Link to="/plan-pricing">
                                <i className="icon nav-icon" data-eva="credit-card"></i>
                                <span className="menu-item" data-key="t-plan-pricing">Plan and pricing</span>
                            </Link>
                        </li>

                        <li className="menu-title"></li>
                        <li>
                            <Link to="/api">
                                <i className="icon nav-icon" data-eva="code-outline"></i>
                                <span className="menu-item" data-key="t-documentation">Api documentation</span>
                            </Link>
                        </li>
                    </ul>
                </div>
            </SimpleBar>
        </React.Fragment>
    );
};

SidebarContent.propTypes = {
    location: PropTypes.object,
};

export default SidebarContent;
