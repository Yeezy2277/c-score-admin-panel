import React, { useEffect, useCallback } from 'react';
import { Outlet, useLocation, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

import {
    changeLayout,
    changeSidebarTheme,
    changeSidebarType,
    changeTopbarTheme,
    changeLayoutWidth,
    showRightSidebarAction,
    changelayoutMode
} from "../../store/actions";

import Header from './Header';
import Sidebar from './Sidebar';
import RightSidebar from "../../components/Common/RightSidebar";

const Layout = ({children}) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const location = useLocation();

    const {
        breadCrumbItems,
        layoutWidth,
        leftSideBarType,
        topbarTheme,
        showRightSidebar,
        leftSideBarTheme,
        layoutMode
    } = useSelector((state) => ({
        breadCrumbItems: state.Breadcrumb.breadCrumbItems,
        leftSideBarType: state.Layout.leftSideBarType,
        layoutWidth: state.Layout.layoutWidth,
        topbarTheme: state.Layout.topbarTheme,
        showRightSidebar: state.Layout.showRightSidebar,
        leftSideBarTheme: state.Layout.leftSideBarTheme,
        layoutMode: state.Layout.layoutMode,
    }));

    const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

    const toggleMenuCallback = () => {
        if (leftSideBarType === "default") {
            dispatch(changeSidebarType("condensed", isMobile));
        } else if (leftSideBarType === "condensed") {
            dispatch(changeSidebarType("default", isMobile));
        }
    };

    const hideRightbar = useCallback((event) => {
        var rightbar = document.getElementById("right-bar");
        if (rightbar && rightbar.contains(event.target)) {
            return;
        } else {
            dispatch(showRightSidebarAction(false));
        }
    }, [dispatch]);

    useEffect(() => {
        document.body.addEventListener("click", hideRightbar, true);
        return () => {
            document.body.removeEventListener("click", hideRightbar, true);
        };
    }, [hideRightbar]);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    useEffect(() => {
        if (layoutMode) {
            dispatch(changelayoutMode(layoutMode));
        }
    }, [leftSideBarTheme, layoutMode, dispatch]);

    useEffect(() => {
        dispatch(changeLayout("vertical"));
    }, [dispatch]);

    useEffect(() => {
        if (leftSideBarTheme) {
            dispatch(changeSidebarTheme(leftSideBarTheme));
        }
    }, [leftSideBarTheme, dispatch]);

    useEffect(() => {
        if (layoutWidth) {
            dispatch(changeLayoutWidth(layoutWidth));
        }
    }, [layoutWidth, dispatch]);

    useEffect(() => {
        if (leftSideBarType) {
            dispatch(changeSidebarType(leftSideBarType));
        }
    }, [leftSideBarType, dispatch]);

    useEffect(() => {
        if (topbarTheme) {
            dispatch(changeTopbarTheme(topbarTheme));
        }
    }, [topbarTheme, dispatch]);

    useEffect(() => {
        const title = location.pathname;
        let currentage = title.charAt(1).toUpperCase() + title.slice(2);
        document.title = currentage + " | C-SCORE";
    }, [location.pathname]);

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return (
        <React.Fragment>
            <div id="layout-wrapper">
                <Header breadCrumbItems={breadCrumbItems} toggleMenuCallback={toggleMenuCallback} />

                <Sidebar
                    theme={leftSideBarTheme}
                    type={leftSideBarType}
                    isMobile={isMobile}
                />
                <div className="main-content">
                    {children}
                </div>
            </div>
            {showRightSidebar ? <RightSidebar /> : null}
        </React.Fragment>
    );
};

export default Layout;
