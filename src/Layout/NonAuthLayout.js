import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

const NonAuthLayout = (props) => {
  const navigate = useNavigate();

  const { layoutMode } = useSelector((state) => ({
    layoutMode: state.Layout.layoutMode,
  }));

  useEffect(() => {
    if (layoutMode === 'dark') {
      document.body.setAttribute('data-layout-mode', 'dark');
    } else {
      document.body.setAttribute('data-layout-mode', 'light');
    }
  }, [layoutMode]);

  // You can navigate to a different route if needed.
  // For example, to redirect to the login page:
  // if (shouldRedirect) {
  //   navigate('/login');
  //   return null;
  // }

  return <React.Fragment>{props.children}</React.Fragment>;
};

export default NonAuthLayout;
