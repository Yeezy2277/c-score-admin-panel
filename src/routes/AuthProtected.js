import React from 'react';
import { Route, Outlet, useLocation, useNavigate } from 'react-router-dom';

import { useProfile } from '../Hooks/UserHooks';

const AuthProtected = () => {
  // const { userProfile, loading } = useProfile();
  const location = useLocation();
  const navigate = useNavigate();

  // Redirect unauthenticated users to the login page
  // if (!userProfile && loading) {
  //   navigate('/signin', { state: { from: location } });
  //   return null;
  // }

  return <Outlet />;
};

const AccessRoute = ({ element: Element, ...rest }) => {
  return <Route {...rest} element={<Element />} />;
};

export { AuthProtected, AccessRoute };
