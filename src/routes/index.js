import React, { createContext, useState } from "react";
import { Outlet, Route, Routes, Navigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { layoutTypes } from "../components/constants/layout";
import NonAuthLayout from "../Layout/NonAuthLayout";
import VerticalLayout from "../Layout/VerticalLayout/Index";
import HorizontalLayout from "../Layout/HorizontalLayout/Index";
import { authProtectedRoutes, publicRoutes } from "./allRoutes";
import { AuthProtected } from "./AuthProtected"; // Import the AuthProtected component

const getLayout = (layoutType) => {
    switch (layoutType) {
        case layoutTypes.VERTICAL:
            return VerticalLayout;
        case layoutTypes.HORIZONTAL:
            return HorizontalLayout;
        default:
            return VerticalLayout; // Default layout
    }
};

export const RouterCacheContext = createContext();

export const RouterCacheProvider = ({ children }) => {
    const [cache, setCache] = useState({});

    return (
        <RouterCacheContext.Provider value={{ cache, setCache }}>
            {children}
        </RouterCacheContext.Provider>
    );
};

const Index = () => {
    const { layoutType } = useSelector((state) => ({
        layoutType: state.Layout.layoutType,
    }));

    const Layout = getLayout(layoutType);

    return (
        <RouterCacheProvider>
            <Routes>
                {/* Define public routes */}
                {publicRoutes.map((route, idx) => (
                    <Route
                        key={idx}
                        path={route.path}
                        element={
                            <NonAuthLayout>
                                <route.component />
                            </NonAuthLayout>
                        }
                    />
                ))}

                {/* Define authenticated routes */}
                {authProtectedRoutes.map((route, idx) => (
                    <Route
                        key={idx}
                        path={route.path}
                        element={
                            <Layout>
                                <route.component />
                            </Layout>
                        }
                    />
                ))}
            </Routes>
        </RouterCacheProvider>
    );
};

export default Index;
