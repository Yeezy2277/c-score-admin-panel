// import React from "react"
import { calenderDefaultCategories, events } from "./calender";
import { chats, messages, contacts, groups } from "./chat";

export {
  events,
  calenderDefaultCategories,
  chats,
  messages,
  contacts,
  groups
};
