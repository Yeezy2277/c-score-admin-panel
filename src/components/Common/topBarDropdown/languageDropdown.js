import React, { useEffect, useState } from "react";;
import {
    Dropdown,
    DropdownItem,
    DropdownMenu,
    DropdownToggle
} from "reactstrap";

import { Link } from 'react-router-dom';
import { get, map } from "lodash";

//i18n
import i18n from "../../../i18n";
import languages from "../../../commonData/languages";

const languageDropdown = () => {
    const [selectedLang, setSelectedLang] = useState("");
    const [languageMenu, setLanguageMenu] = useState("");

    const toggle = () => {
        setLanguageMenu(!languageMenu);
    };

    useEffect(() => {
        const currentLanguage = localStorage.getItem("I18N_LANGUAGE");
        setSelectedLang(currentLanguage);
    }, []);


    return (
        <Dropdown isOpen={languageMenu} toggle={toggle} className="d-inline-block language-switch">
            <DropdownToggle type="button" className="btn header-item" tag="button">
                <img className="header-lang-img" src={usFlag} alt="Header Language" height="16" />
            </DropdownToggle>

            <DropdownMenu className="dropdown-menu-end">
                {language.map((languages, key) => (
                    <DropdownItem key={key} to="#" className="notify-item language" data-lang="eng">
                        <img src={languages.flagImage} alt="user-image" className="me-1" height="12" />
                        <span className="align-middle">{languages.languageName}</span>
                    </DropdownItem>
                ))}
            </DropdownMenu>
        </Dropdown>
    );
}; 