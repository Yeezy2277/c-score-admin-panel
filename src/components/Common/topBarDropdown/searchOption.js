import React from "react";
import {
    Input
  } from "reactstrap";

const searchOption=()=>{
                        <div className="dropdown">
                            <button type="button" className="btn header-item"
                                data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="icon-sm" data-eva="search-outline"></i>
                            </button>
                            <div className="dropdown-menu dropdown-menu-end dropdown-menu-md p-0">
                                <form className="p-2">
                                    <div className="search-box">
                                        <div className="position-relative">
                                            <Input type="text" className="form-control bg-light border-0" placeholder="Search..." />
                                            <i className="search-icon" data-eva="search-outline" data-eva-height="26" data-eva-width="26"></i>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
}

export default searchOption;