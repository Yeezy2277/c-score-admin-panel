import { useEffect } from "react";
import { useSupabaseClient } from "@supabase/auth-helpers-react";
import {defineDatePeriod, getAggregatedData, getDashboardsFinalData} from "../helpers/dashboard_helpers";

export const useGetChartsData = ({ supabaseDataFilter,  setChartsData}) => {
    const supabaseClient = useSupabaseClient();

    useEffect(() => {
        (async () => {
            const { data: fetchedSupabaseData } = await supabaseClient
                .from('test_gamblers_aggr')
                .select("*")
                .eq(supabaseDataFilter.dataKey, supabaseDataFilter.dataValue);
            const resultData = Object.keys(fetchedSupabaseData).map(item => (
                fetchedSupabaseData[item]
            ))
                .sort((a, b) => (new Date(a.created_at).getTime() - new Date(b.created_at).getTime()));
            const uniqueDates = new Set(resultData.map(item => item.created_at));
            const uniqueLocales = new Set(resultData.map(item => item.locale).filter(item => item));

            const rawDataForLineChart = {};
            const rawDataForPieChart = {};

            for (let singleDate of uniqueDates.values()) {
                resultData.forEach(item => {
                    if (item.created_at === singleDate) {
                        const objKey = item.created_at.split("T")[0];
                        const objSpread = rawDataForLineChart[objKey] ? rawDataForLineChart[objKey] : [];
                        Object.assign(rawDataForLineChart, { [objKey]: [...objSpread, item] });
                    }
                });
            }

            for (let singleLocale of uniqueLocales) {
                resultData.forEach(item => {
                    if (item.locale === singleLocale && item.suspicious) {
                        Object.assign(rawDataForPieChart, {
                            [singleLocale]: (rawDataForPieChart[singleLocale] || 0) + item.count_gamblers
                        });
                    }
                });
            }
            const currentDate = new Date();
            const currentYear = currentDate.getFullYear();
            const currentMonth = currentDate.getMonth();
            const oneDayInMilliseconds = 24 * 60 * 60 * 1000;
            const { startDate, endDate } = defineDatePeriod({
                supabaseDataFilter,
                currentYear,
                currentMonth,
                currentDate,
                oneDayInMilliseconds
            });
            const aggregatedData = getAggregatedData({
                startDate, endDate, tempData: rawDataForLineChart, oneDayInMilliseconds
            });
            const finalData = getDashboardsFinalData(aggregatedData);
            setChartsData({
                dateRange: {
                    startDate,
                    endDate
                },
                lineChartData: finalData,
                pieChartData: rawDataForPieChart
            });
        })();
    }, [supabaseDataFilter]);
};
