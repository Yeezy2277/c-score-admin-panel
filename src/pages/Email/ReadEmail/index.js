import React, { useEffect } from 'react';
import { Col, Container, Row } from 'reactstrap';

// redux
import { useDispatch } from "react-redux";
import { setBreadcrumb } from "../../../store/actions";
import EmailSidebar from '../EmailSidebar';
import EmailRead from "./ReadEmail";

const ReadEmail = () => {
    const dispatch = useDispatch();
    /*
    set breadcrumbs
    */
    useEffect(() => {
        const breadCrumbItems = {
            title: "Inbox",
        };
        dispatch(setBreadcrumb(breadCrumbItems));
    }, [dispatch]);
    return (
        <React.Fragment>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col xs={12}>
                            <EmailSidebar />
                            <EmailRead />
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default ReadEmail;