import React, {useEffect, useState} from 'react';
import { Card, CardBody, CardHeader, Col, Container, Row } from 'reactstrap';
import MetaTags from "react-meta-tags";

// redux
import { useDispatch } from "react-redux";
import { setBreadcrumb } from "../../store/actions";

const AccountInfo = () => {
    const dispatch = useDispatch();
    const [isToggle, setIsToggle] = useState(false);
    console.log(isToggle);
    const changeToggleState = () => setIsToggle(prevState => !prevState);

    /*
    set breadcrumbs
    */

    useEffect(() => {
        const breadCrumbItems = {
            title: "Account Info",
        };
        dispatch(setBreadcrumb(breadCrumbItems));
    }, [dispatch]);
    return (
        <React.Fragment>
            <MetaTags>
                <title>Account Info | C-SCORE </title>
            </MetaTags>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col className="col-12">
                            <Card>
                                <CardBody>
                                    <Row className="mb-3">
                                        <label htmlFor="example-text-input" className="col-md-2 col-form-label">Name</label>
                                        <Col md={10}>
                                            <input className="form-control" type="text" placeholder="Artisanal kale"
                                                   id="example-text-input" />
                                        </Col>
                                    </Row>
                                    <Row className="mb-3">
                                        <label htmlFor="example-search-input" className="col-md-2 col-form-label">Current plan</label>
                                        <Col md={10}>
                                            <input className="form-control" type="search" placeholder="How do I shoot web"
                                                   id="example-search-input" />
                                        </Col>
                                    </Row>
                                    <Row className="mb-3">
                                        <label htmlFor="example-email-input" className="col-md-2 col-form-label">Email</label>
                                        <Col md={10}>
                                            <input className="form-control" type="email" placeholder="bootstrap@example.com"
                                                   id="example-email-input" />
                                        </Col>
                                    </Row>
                                    <Row className="mb-3">
                                        <label htmlFor="example-url-input" className="col-md-2 col-form-label">API Key</label>
                                        <Col md={10}>
                                            <input className="form-control" type="url" placeholder="https://getbootstrap.com"
                                                   id="example-url-input" />
                                        </Col>
                                    </Row>
                                    <Row className="mb-3 align-items-center">
                                        <label htmlFor="example-tel-input" className="col-md-2 col-form-label">Notification</label>
                                        <Col md={10}>
                                            <button onClick={changeToggleState} className="btn"><i className="icon nav-icon" data-eva={`toggle-${isToggle ? "left" : "right"}-outline`}></i></button>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default AccountInfo;