// Import Images
import logodark from "../../assets/images/logo-dark.svg";
import logolight from "../../assets/images/logo-light.svg";
import React, { useEffect } from 'react';
import Icon from 'react-eva-icons';
import MetaTags from 'react-meta-tags';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card, CardBody, Col, Container, Row } from 'reactstrap';
import AuthSlider from './authCarousel';
import { useSupabaseClient } from '@supabase/auth-helpers-react';
import { useForm } from 'react-hook-form';
import { useAuthRedirectUrl } from '../../lib/client/auth';

const RecoverPassword = () => {
    const { layoutMode } = useSelector((state) => ({
        layoutMode: state.Layout.layoutMode,
    }));
    const supabaseClient = useSupabaseClient();
    const redirectTo = useAuthRedirectUrl(`/auth/callback?redirectAfterSignin=${encodeURIComponent('/auth/reset-password')}`);
    const { register, handleSubmit, formState, setError, clearErrors } = useForm();
    const { isSubmitting, isSubmitted, isSubmitSuccessful } = formState;

    const onSubmit = async ({ email }) => {
        clearErrors('serverError');
        try {
            const { error, data } = await supabaseClient.auth.resetPasswordForEmail(email, { redirectTo });
            if (error) {
                console.log(error);
                setError('serverError', { type: 'manual', message: error.message });
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        if (layoutMode === 'dark') {
            document.body.classList.remove('bg-transparent');
        } else {
            document.body.className = 'bg-transparent';
        }

        // Remove classname when component will unmount
        return function cleanup() {
            document.body.className = '';
        };
    }, [layoutMode]);

    return (
        <React.Fragment>
            <MetaTags>
                <title>Recover Password | C-SCORE</title>
            </MetaTags>
            <div className="auth-page">
                <Container fluid className="p-0">
                    <Row className="g-0 align-items-center justify-content-center">
                        <Col className="col-6">
                            <Row className="justify-content-center g-0">
                                <Col xl={9}>
                                    <div className="p-4">
                                        <Card className="mb-0">
                                            <CardBody>
                                                <div className="auth-full-page-content rounded d-flex p-3 my-2">
                                                    <div className="w-100">
                                                        <div className="d-flex flex-column h-100">
                                                            <div className="mb-4 mb-md-5">
                                                                <Link to="/index" className="d-block auth-logo">
                                                                    <img src={logodark} alt="" height="100" className="auth-logo-dark me-start" />
                                                                    <img src={logolight} alt="" height="100" className="auth-logo-light me-start" />
                                                                </Link>
                                                            </div>

                                                            <div className="auth-content my-auto">
                                                                <div className="text-center">
                                                                    <h5 className="mb-0">Forgot your Password?</h5>
                                                                </div>
                                                                <div className="alert alert-success text-center my-4 font-size-12" role="alert">
                                                                    Enter your Email and instructions will be sent to you!
                                                                </div>
                                                                <form className="mt-4 pt-2" onSubmit={handleSubmit(onSubmit)}>
                                                                    <div className="form-floating form-floating-custom mb-4">
                                                                        <input
                                                                            type="email"
                                                                            className="form-control"
                                                                            id="input-email"
                                                                            placeholder="Enter Email"
                                                                            required
                                                                            {...register('email', { required: true })}
                                                                        />
                                                                        <div className="invalid-feedback">Please Enter Email</div>
                                                                        <label htmlFor="input-email">Email</label>
                                                                        <div className="form-floating-icon">
                                                                            <Icon name="email-outline" fill="#555b6d" />
                                                                        </div>
                                                                    </div>
                                                                    <div className="mb-3">
                                                                        <button
                                                                            className="btn btn-primary w-100 waves-effect waves-light"
                                                                            type="submit"
                                                                            disabled={isSubmitting}
                                                                        >
                                                                            {isSubmitting ? 'Resetting...' : 'Reset'}
                                                                        </button>
                                                                    </div>
                                                                </form>

                                                                <div className="mt-4 pt-3 text-center">
                                                                    <p className="text-muted mb-0">Remember It ? Back to <Link to="/signin" className="text-primary fw-semibold"> Sign In </Link> </p>
                                                                </div>
                                                            </div>

                                                            <div className="mt-4 text-center">
                                                                <p className="mb-0">© C-SCORE Tech. All rights reserved</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </CardBody>
                                        </Card>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                        <AuthSlider />
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default RecoverPassword;
