// Import Images
import logodark from "../../assets/images/logo-dark.svg";
import logolight from "../../assets/images/logo-light.svg";
import React, { useEffect, useState } from 'react';
import Icon from 'react-eva-icons';
import MetaTags from 'react-meta-tags';
import { useSupabaseClient } from '@supabase/auth-helpers-react';
import { useForm } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { Link, Outlet, useLocation, useNavigate } from 'react-router-dom';
import { Card, CardBody, Col, Container, Row } from 'reactstrap';
import AuthSlider from './authCarousel';
import { useAuthRedirectUrl } from '../../lib/client/auth';
import { useRouterCache } from '../../helpers/auth_helper';

const Login = () => {
    const { layoutMode } = useSelector((state) => ({
        layoutMode: state.Layout.layoutMode,
    }));

    const [mode, setMode] = useState('password');
    const { cache, invalidateCache } = useRouterCache();
    const supabaseClient = useSupabaseClient();
    const location = useLocation();
    const navigate = useNavigate();

    const query = new URLSearchParams(location.search);
    const token = query.get('token')
    const type = query.get('type')
    console.log(token, type);

    const redirectPath = '/'; // Replace with your default redirect path

    const redirectAfterSignin = query.get('redirectAfterSignin')
        ? decodeURIComponent(query.get('redirectAfterSignin'))
        : redirectPath;
    console.log(redirectAfterSignin);

    const redirectTo = useAuthRedirectUrl(
        mode === 'magic-link'
            ? `/auth/callback?redirectAfterSignin=${encodeURIComponent(redirectAfterSignin)}`
            : redirectAfterSignin
    );

    const {
        register,
        handleSubmit,
        setError,
        clearErrors,
        formState: { isSubmitting, isSubmitted, isSubmitSuccessful },
        reset,
    } = useForm();

    const signInGoogle = () => supabaseClient.auth.signInWithOAuth({ provider: "google", options: { redirectTo } });

    const onSubmit = async (data) => {
        clearErrors('serverError');
        const { email, password } = data;
        try {
            const { error, data } =
                mode === 'password'
                    ? await supabaseClient.auth.signInWithPassword({ email, password })
                    : await supabaseClient.auth.signInWithOtp({
                        email,
                        options: {
                            emailRedirectTo: redirectTo,
                        },
                    });

            if (error) {
                setError('serverError', {
                    type: 'invalidCredentials',
                });
                return;
            }

            if (mode === 'password') {
                invalidateCache();
                console.log("redirect")
                navigate("/");
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        if (layoutMode === 'dark') {
            document.body.classList.remove('bg-transparent');
        } else {
            document.body.className = 'bg-transparent';
        }

        // Remove classname when component will unmount
        return function cleanup() {
            document.body.className = '';
        };
    }, [layoutMode]);

    return (
        <React.Fragment>
            <MetaTags>
                <title>Login | C-SCORE </title>
            </MetaTags>
            <div className="auth-page">
                <Container fluid className="p-0">
                    <Row className="g-0 align-items-center justify-content-center">
                        <Col className="col-6">
                            <Row className="justify-content-center g-0">
                                <Col xl={9}>
                                    <div className="p-4">
                                        <Card className="mb-0">
                                            <CardBody>
                                                <div className="auth-full-page-content rounded d-flex p-3 my-2">
                                                    <div className="w-100">
                                                        <div className="d-flex flex-column h-100">
                                                            <div className="mb-4 mb-md-5">
                                                                <Link to="/index" className="d-block auth-logo">
                                                                    <img src={logodark} alt="" height="100" className="auth-logo-dark me-start" />
                                                                    <img src={logolight} alt="" height="100" className="auth-logo-light me-start" />
                                                                </Link>
                                                            </div>

                                                            <div className="auth-content my-auto">
                                                                <div className="text-center">
                                                                    <h5 className="mb-0">Welcome Back !</h5>
                                                                    <p className="text-muted mt-2 mb-0">Enter your credentials to sign in.</p>
                                                                    <p className="text-muted">
                                                                        Don't have an account yet?{' '}
                                                                        <Link to="/signup" className="text-primary fw-semibold">
                                                                            {' '}
                                                                            Create an account{' '}
                                                                        </Link>
                                                                    </p>
                                                                </div>
                                                                <form className="mt-4 pt-2" onSubmit={handleSubmit(onSubmit)}>
                                                                    <div className="form-floating form-floating-custom mb-4">
                                                                        <input
                                                                            type="text"
                                                                            className="form-control"
                                                                            id="input-username"
                                                                            placeholder="Enter User Name"
                                                                            {...register('email')}
                                                                        />
                                                                        <label htmlFor="input-username">Email</label>
                                                                        <div className="form-floating-icon">
                                                                            <Icon name="people-outline" fill="#555b6d" />
                                                                        </div>
                                                                    </div>

                                                                    <div className="form-floating form-floating-custom mb-4 auth-pass-inputgroup">
                                                                        <input
                                                                            type="password"
                                                                            className="form-control pe-5"
                                                                            id="password-input"
                                                                            placeholder="Enter Password"
                                                                            {...register('password')}
                                                                        />

                                                                        <button
                                                                            type="button"
                                                                            className="btn btn-link position-absolute h-100 end-0 top-0"
                                                                            id="password-addon"
                                                                        >
                                                                            <i className="mdi mdi-eye-outline font-size-18 text-muted"></i>
                                                                        </button>
                                                                        <label htmlFor="password-input">Password</label>
                                                                        <div className="form-floating-icon">
                                                                            <Icon name="lock-outline" fill="#555b6d" />
                                                                        </div>
                                                                    </div>

                                                                    <div className="row mb-4">
                                                                        <div className="col">
                                                                            <Link to="/forgot-password" className="text-primary fw-semibold">
                                                                                {' '}
                                                                                Forgot your password?{' '}
                                                                            </Link>
                                                                        </div>
                                                                    </div>
                                                                    <div className="mb-3">
                                                                        <button
                                                                            className="btn btn-primary w-100 waves-effect waves-light"
                                                                            type="submit"
                                                                            disabled={isSubmitting}
                                                                        >
                                                                            {isSubmitting ? 'Logging in...' : 'Log In'}
                                                                        </button>
                                                                    </div>
                                                                </form>

                                                                <div className="mt-4 pt-3 text-center">
                                                                    <div className="signin-other-title">
                                                                        <h5 className="font-size-14 mb-4 text-muted fw-medium">- Or you can join with -</h5>
                                                                    </div>

                                                                    <div className="d-flex gap-2">
                                                                        <button onClick={signInGoogle} type="button" className="btn btn-soft-danger waves-effect waves-light w-100">
                                                                            <i className="bx bxl-google font-size-16 align-middle"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div className="mt-4 text-center">
                                                                <p className="mb-0">© C-SCORE Tech. All rights reserved</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </CardBody>
                                        </Card>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                        <AuthSlider />
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default Login;
