// Import Images
import logodark from "../../assets/images/logo-dark.svg";
import logolight from "../../assets/images/logo-light.svg";
import React, {useCallback, useEffect} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Icon from 'react-eva-icons';
import MetaTags from 'react-meta-tags';
import {Provider, useSelector} from 'react-redux';
import { useForm } from 'react-hook-form';
import { Card, CardBody, Col, Container, Row } from 'reactstrap';
import AuthSlider from './authCarousel';
import { useSupabaseClient } from '@supabase/auth-helpers-react';
import {useAuthRedirectUrl} from "../../lib/client/auth";

const ConfirmRegister = () => {
    const { layoutMode } = useSelector((state) => ({
        layoutMode: state.Layout.layoutMode,
    }));
    const navigate = useNavigate();
    const supabaseClient = useSupabaseClient();
    const redirectTo = useAuthRedirectUrl();
    const {
        handleSubmit,
        formState: { isSubmitting, isSubmitted, isSubmitSuccessful },
        register,
        setError,
        clearErrors,
    } = useForm({
        defaultValues: {
            email: '',
            password: '',
            name: '',
        },
    });

    const signInGoogle = () => supabaseClient.auth.signInWithOAuth({ provider: "google", options: { redirectTo } });

    const onSubmit = async ({ email, password, name }) => {
        clearErrors('serverError');
        try {
            const {
                data: { session, user: newUser },
                error,
            } = await supabaseClient.auth.signUp({
                email,
                password,
                options: {
                    data: { full_name: name },
                    redirectTo: '/',
                },
            });

            if (error || !newUser) {
                setError('serverError', { message: error?.message });
                return;
            }

            // if email confirmations are enabled, the user will have to confirm their email before they can sign in
            if (!session) return;

            navigate('/');
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        if (layoutMode === 'dark') {
            document.body.classList.remove('bg-transparent');
        } else {
            document.body.className = 'bg-transparent';
        }

        // Remove classname when component will unmount
        return function cleanup() {
            document.body.className = '';
        };
    }, [layoutMode]);

    return (
        <React.Fragment>
            <MetaTags>
                <title>Register | C-SCORE</title>
            </MetaTags>
            <div className="auth-page">
                <Container fluid className="p-0">
                    <Row className="g-0 align-items-center justify-content-center">
                        <Col className="col-6">
                            <Row className="justify-content-center g-0">
                                <Col xl={9}>
                                    <div className="p-4">
                                        <Card className="mb-0">
                                            <CardBody>
                                                <div className="auth-full-page-content rounded d-flex p-3 my-2">
                                                    <div className="w-100">
                                                        <div className="d-flex flex-column h-100">
                                                            <div className="mb-4 mb-md-5">
                                                                <a href="/" className="d-block auth-logo">
                                                                    <img src={logodark} alt="" height="100" className="auth-logo-dark me-start" />
                                                                    <img src={logolight} alt="" height="100" className="auth-logo-light me-start" />
                                                                </a>
                                                            </div>

                                                            <div className="auth-content my-auto">
                                                                <div className="text-center">
                                                                    <h5 className="mb-0">Confirm an account</h5>
                                                                    <p className="text-muted mt-2">
                                                                        Already have an account?{' '}
                                                                        <Link to="/signin" className="text-primary fw-semibold">
                                                                            Sign In
                                                                        </Link>
                                                                    </p>
                                                                    <div className="alert alert-success text-center my-4 font-size-12" role="alert">
                                                                        Thank you for signing up! Please check your email to confirm your registration
                                                                    </div>
                                                                </div>

                                                            </div>

                                                            <div className="mt-4 text-center">
                                                                <p className="mb-0">© C-SCORE Tech. All rights reserved</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </CardBody>
                                        </Card>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                        <AuthSlider />
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default ConfirmRegister;
