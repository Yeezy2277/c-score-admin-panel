// Import Images
import logodark from "../../assets/images/logo-dark.svg";
import logolight from "../../assets/images/logo-light.svg";
import React, {useCallback, useEffect} from 'react';
import {Link, useNavigate} from 'react-router-dom';
import Icon from 'react-eva-icons';
import MetaTags from 'react-meta-tags';
import {Provider, useSelector} from 'react-redux';
import { useForm } from 'react-hook-form';
import { Card, CardBody, Col, Container, Row } from 'reactstrap';
import AuthSlider from './authCarousel';
import { useSupabaseClient } from '@supabase/auth-helpers-react';
import {useAuthRedirectUrl} from "../../lib/client/auth";

const Register = () => {
    const { layoutMode } = useSelector((state) => ({
        layoutMode: state.Layout.layoutMode,
    }));
    const navigate = useNavigate();
    const supabaseClient = useSupabaseClient();
    const redirectTo = useAuthRedirectUrl();
    const {
        handleSubmit,
        formState: { isSubmitting, isSubmitted, isSubmitSuccessful },
        register,
        setError,
        clearErrors,
    } = useForm({
        defaultValues: {
            email: '',
            password: '',
            name: '',
        },
    });
    //https://jitboeryveavgzphlkgr.supabase.co/auth/v1/verify?token=eb29b42cbd999e1866155f30007ea20eaf370952ee748a7a23999c48&type=signup&redirect_to=https://csadmin-proto-git-master-cscore.vercel.app/

    const signInGoogle = () => supabaseClient.auth.signInWithOAuth({ provider: "google", options: { redirectTo } });

    const onSubmit = async ({ email, password, name }) => {
        clearErrors('serverError');
        try {
            const {
                data: { user: newUser },
                error,
            } = await supabaseClient.auth.signUp({
                email,
                password,
                options: {
                    data: { full_name: name },
                },
            });

            if (error || !newUser) {
                setError('serverError', { message: error?.message });
                return;
            }

            navigate('/signup-confirm');

        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        if (layoutMode === 'dark') {
            document.body.classList.remove('bg-transparent');
        } else {
            document.body.className = 'bg-transparent';
        }

        // Remove classname when component will unmount
        return function cleanup() {
            document.body.className = '';
        };
    }, [layoutMode]);

    return (
        <React.Fragment>
            <MetaTags>
                <title>Register | C-SCORE</title>
            </MetaTags>
            <div className="auth-page">
                <Container fluid className="p-0">
                    <Row className="g-0 align-items-center justify-content-center">
                        <Col className="col-6">
                            <Row className="justify-content-center g-0">
                                <Col xl={9}>
                                    <div className="p-4">
                                        <Card className="mb-0">
                                            <CardBody>
                                                <div className="auth-full-page-content rounded d-flex p-3 my-2">
                                                    <div className="w-100">
                                                        <div className="d-flex flex-column h-100">
                                                            <div className="mb-4 mb-md-5">
                                                                <a href="/" className="d-block auth-logo">
                                                                    <img src={logodark} alt="" height="100" className="auth-logo-dark me-start" />
                                                                    <img src={logolight} alt="" height="100" className="auth-logo-light me-start" />
                                                                </a>
                                                            </div>

                                                            <div className="auth-content my-auto">
                                                                <div className="text-center">
                                                                    <h5 className="mb-0">Register an account</h5>
                                                                    <p className="text-muted mt-2">
                                                                        Enter your information to create a new account. Already have an account?{' '}
                                                                        <Link to="/signin" className="text-primary fw-semibold">
                                                                            Sign In
                                                                        </Link>
                                                                    </p>
                                                                </div>
                                                                <form className="mt-4 pt-2" onSubmit={handleSubmit(onSubmit)}>
                                                                    <div className="form-floating form-floating-custom mb-4">
                                                                        <input
                                                                            type="email"
                                                                            className="form-control"
                                                                            id="input-email"
                                                                            placeholder="Enter Email"
                                                                            required
                                                                            {...register('email')}
                                                                        />
                                                                        <div className="invalid-feedback">Please Enter Email</div>
                                                                        <label htmlFor="input-email">Email</label>
                                                                        <div className="form-floating-icon">
                                                                            <Icon name="email-outline" fill="#555b6d" />
                                                                        </div>
                                                                    </div>

                                                                    <div className="form-floating form-floating-custom mb-4">
                                                                        <input
                                                                            type="text"
                                                                            className="form-control"
                                                                            id="input-username"
                                                                            placeholder="Enter User Name"
                                                                            required
                                                                            {...register('name')}
                                                                        />
                                                                        <div className="invalid-feedback">Please Enter Username</div>
                                                                        <label htmlFor="input-username">Username</label>
                                                                        <div className="form-floating-icon">
                                                                            <Icon name="people-outline" fill="#555b6d" />
                                                                        </div>
                                                                    </div>

                                                                    <div className="form-floating form-floating-custom mb-4">
                                                                        <input
                                                                            type="password"
                                                                            className="form-control"
                                                                            id="input-password"
                                                                            placeholder="Enter Password"
                                                                            required
                                                                            {...register('password')}
                                                                        />
                                                                        <div className="invalid-feedback">Please Enter Password</div>
                                                                        <label htmlFor="input-password">Password</label>
                                                                        <div className="form-floating-icon">
                                                                            <Icon name="lock-outline" fill="#555b6d" />
                                                                        </div>
                                                                    </div>

                                                                    <div className="mb-4">
                                                                        <p className="mb-0">By registering you agree to the C-SCORE{' '}
                                                                            <a to="#" className="text-primary">
                                                                                Terms of Use
                                                                            </a>
                                                                        </p>
                                                                    </div>
                                                                    <div className="mb-3">
                                                                        <button
                                                                            className="btn btn-primary w-100 waves-effect waves-light"
                                                                            type="submit"
                                                                            disabled={isSubmitting}
                                                                        >
                                                                            {isSubmitting ? 'Registering...' : 'Register'}
                                                                        </button>
                                                                    </div>
                                                                </form>

                                                                <div className="mt-4 pt-3 text-center">
                                                                    <div className="signin-other-title">
                                                                        <h5 className="font-size-14 mb-4 text-muted fw-medium">- Or you can join with -</h5>
                                                                    </div>

                                                                    <div className="d-flex gap-2">
                                                                        <button onClick={signInGoogle} type="button" className="btn btn-soft-danger waves-effect waves-light w-100">
                                                                            <i className="bx bxl-google font-size-16 align-middle"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>

                                                                <div className="mt-4 pt-3 text-center">
                                                                    <p className="text-muted mb-0">
                                                                        Already have an account?{' '}
                                                                        <a to="/signin" className="text-primary fw-semibold">
                                                                            Login
                                                                        </a>
                                                                    </p>
                                                                </div>
                                                            </div>

                                                            <div className="mt-4 text-center">
                                                                <p className="mb-0">© C-SCORE Tech. All rights reserved</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </CardBody>
                                        </Card>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                        <AuthSlider />
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default Register;
