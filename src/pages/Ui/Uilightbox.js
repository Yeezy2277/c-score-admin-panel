import React, { useState, useEffect } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Container, Row } from 'reactstrap';
import MetaTags from "react-meta-tags";

// redux
import { useDispatch } from "react-redux";
import { setBreadcrumb } from "../../store/actions";

//Lightbox
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import ModalVideo from "react-modal-video";
import "react-modal-video/scss/modal-video.scss";

// Import Images
import img1 from '../../assets/images/small/img-1.jpg';
import img2 from '../../assets/images/small/img-2.jpg';
import img3 from '../../assets/images/small/img-3.jpg';
import img4 from '../../assets/images/small/img-4.jpg';
import img5 from '../../assets/images/small/img-5.jpg';
import img6 from '../../assets/images/small/img-6.jpg';
import { Link } from 'react-router-dom';

const images = [img1, img2, img3, img4, img5, img6];
const imageZoom = [img4, img5, img1];

const Uilightbox = () => {

    const [photoIndex, setphotoIndex] = useState(0);
    const [isGallery, setisGallery] = useState(false);
    const [isGalleryZoom, setisGalleryZoom] = useState(false);
    const [isOpen, setisOpen] = useState(false);
    const [isOpen1, setisOpen1] = useState(false);


    const dispatch = useDispatch();

    /*
    set breadcrumbs
    */

    useEffect(() => {
        const breadCrumbItems = {
            title: "Lightbox",
        };
        dispatch(setBreadcrumb(breadCrumbItems));
    }, [dispatch]);
    return (
        <React.Fragment>
            <MetaTags>
                <title>Lightbox | C-SCORE </title>
            </MetaTags>
            <div className="page-content">
                <Container fluid>

                    {isGallery ? (
                        <Lightbox
                            mainSrc={images[photoIndex]}
                            nextSrc={images[(photoIndex + 1) % images.length]}
                            prevSrc={images[(photoIndex + images.length - 1) % images.length]}
                            enableZoom={true}
                            onCloseRequest={() => {
                                setisGallery(false);
                            }}
                            onMovePrevRequest={() => {
                                setphotoIndex((photoIndex + images.length - 1) % images.length);
                            }}
                            onMoveNextRequest={() => {
                                setphotoIndex((photoIndex + 1) % images.length);
                            }}
                            imageCaption={"Project " + parseFloat(photoIndex + 1)}
                        />
                    ) : null}

                    {isGalleryZoom ? (
                        <Lightbox
                            mainSrc={imageZoom[photoIndex]}
                            nextSrc={imageZoom[(photoIndex + 1) % imageZoom.length]}
                            prevSrc={imageZoom[(photoIndex + imageZoom.length - 1) % imageZoom.length]}
                            onCloseRequest={() => {
                                setisGalleryZoom(false);
                            }}
                            onMovePrevRequest={() => {
                                setphotoIndex((photoIndex + imageZoom.length - 1) % imageZoom.length);
                            }}
                            onMoveNextRequest={() => {
                                setphotoIndex((photoIndex + 1) % imageZoom.length);
                            }}
                        />
                    ) : null}
                    <Row>
                        <Col xl={12}>
                            <Card>
                                <CardHeader>
                                    <h4 className="card-title"> Lightbox gallery</h4>
                                </CardHeader>
                                <CardBody>
                                    <p className="card-title-desc mb-0">In this example lazy-loading of images is enabled for
                                        the next image based on move direction.</p>
                                    <div className="popup-gallery">
                                        <Row>
                                            <Col xl={2} md={4} className="col-6">
                                                <div className="mt-4">
                                                    <Link to="assets/images/small/img-1.jpg" className="thumb preview-thumb image-popup">
                                                        <div className="img-fluid">
                                                            <img src={img1} alt="" className="img-fluid d-block" onClick={() => { setisGallery(true); setphotoIndex(0); }} />
                                                        </div>
                                                    </Link>
                                                </div>
                                            </Col>
                                            <Col xl={2} md={4} className="col-6">
                                                <div className="mt-4">
                                                    <Link to="assets/images/small/img-2.jpg" className="thumb preview-thumb image-popup">
                                                        <div className="img-fluid">
                                                            <img src={img2} alt="" className="img-fluid d-block" onClick={() => { setisGallery(true); setphotoIndex(1); }} />
                                                        </div>
                                                    </Link>
                                                </div>
                                            </Col>
                                            <Col xl={2} md={4} className="col-6">
                                                <div className="mt-4">
                                                    <Link to="assets/images/small/img-3.jpg" className="thumb preview-thumb image-popup">
                                                        <div className="img-fluid">
                                                            <img src={img3} alt="" className="img-fluid d-block" onClick={() => { setisGallery(true); setphotoIndex(2); }} />
                                                        </div>
                                                    </Link>
                                                </div>
                                            </Col>
                                            <Col xl={2} md={4} className="col-6">
                                                <div className="mt-4">
                                                    <Link to="assets/images/small/img-4.jpg" className="thumb preview-thumb image-popup">
                                                        <div className="img-fluid">
                                                            <img src={img4} alt="" className="img-fluid d-block" onClick={() => { setisGallery(true); setphotoIndex(3); }} />
                                                        </div>
                                                    </Link>
                                                </div>
                                            </Col>
                                            <Col xl={2} md={4} className="col-6">
                                                <div className="mt-4">
                                                    <Link to="assets/images/small/img-5.jpg" className="thumb preview-thumb image-popup">
                                                        <div className="img-fluid">
                                                            <img src={img5} alt="" className="img-fluid d-block" onClick={() => { setisGallery(true); setphotoIndex(4); }} />
                                                        </div>
                                                    </Link>
                                                </div>
                                            </Col>
                                            <Col xl={2} md={4} className="col-6">
                                                <div className="mt-4">
                                                    <Link to="assets/images/small/img-6.jpg" className="thumb preview-thumb image-popup">
                                                        <div className="img-fluid">
                                                            <img src={img6} alt="" className="img-fluid d-block" onClick={() => { setisGallery(true); setphotoIndex(5); }} />
                                                        </div>
                                                    </Link>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>


                    <Row>
                        <Col xl={6}>
                            <Card>
                                <CardHeader>
                                    <h4 className="card-title"> Images with Description</h4>
                                </CardHeader>
                                <CardBody>
                                    <Row>
                                        <Col lg={3} sm={6}>
                                            <div className="">
                                                <Link to="assets/images/small/img-4.jpg" className="thumb preview-thumb image-popup-desc" data-title="Project 01" data-description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit">
                                                    <img src={img4} className="img-fluid" alt="work-thumbnail" onClick={() => { setisGalleryZoom(true); setphotoIndex(0); }} />
                                                </Link>
                                            </div>
                                        </Col>

                                        <Col lg={3} sm={6}>
                                            <div className="mt-4 mt-md-0">
                                                <Link to="assets/images/small/img-5.jpg" className="thumb preview-thumb image-popup-desc" data-title="Project 02" data-description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit">
                                                    <img src={img5} className="img-fluid" alt="work-thumbnail" onClick={() => { setisGalleryZoom(true); setphotoIndex(1); }} />
                                                </Link>
                                            </div>
                                        </Col>

                                        <Col lg={3} sm={6}>
                                            <div className="mt-4 mt-lg-0">
                                                <Link to="assets/images/small/img-1.jpg" className="thumb preview-thumb image-popup-desc" data-title="Project 03" data-description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit">
                                                    <img src={img1} className="img-fluid" alt="work-thumbnail" onClick={() => { setisGalleryZoom(true); setphotoIndex(2); }} />
                                                </Link>
                                            </div>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>

                        <Col xl={6}>
                            <Card>
                                <CardHeader>
                                    <h4 className="card-title">Popup with Video or Map</h4>
                                </CardHeader>
                                <CardBody>
                                    <p className="card-title-desc">In this example lazy-loading of images is enabled for the
                                        next image based on move direction. </p>
                                    <Row>
                                        <div className="col-12">
                                            <div className="button-items">
                                                <Button
                                                    color="light"
                                                    className="image-popup-video-map btn btn-light mo-mb-2"
                                                    onClick={() => {
                                                        setisOpen(!isOpen);
                                                    }}
                                                >
                                                    Open YouTube Video
                                                </Button>{" "}
                                                <Button
                                                    color="light"
                                                    className="image-popup-video-map btn btn-light mo-mb-2"
                                                    onClick={() => {
                                                        setisOpen1(!isOpen1);
                                                    }}
                                                >
                                                    Open Vimeo Video
                                                </Button>{" "}


                                                <ModalVideo
                                                    videoId="L61p2uyiMSo"
                                                    channel="youtube"
                                                    isOpen={isOpen}
                                                    onClose={() => {
                                                        setisOpen(!isOpen);
                                                    }}
                                                />
                                                <ModalVideo
                                                    videoId="L61p2uyiMSo"
                                                    channel="youtube"
                                                    isOpen={isOpen1}
                                                    onClose={() => {
                                                        setisOpen1(false);
                                                    }}
                                                />
                                            </div>
                                        </div>
                                    </Row>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default Uilightbox;