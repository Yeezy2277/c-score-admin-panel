import React, {useEffect, useState} from 'react';
import { Col, Container, Row } from 'reactstrap';
import MetaTags from "react-meta-tags";

// redux
import { useDispatch } from "react-redux";
import { setBreadcrumb } from "../../store/actions";

import Overview from './Overview';
import RatingAndReviews from './RatingAndReviews';
import UserActivity from './UserActivity';
import UserSidebar from './UserSidebar';
import Widgets from './Widgets';
import Transaction from './Transaction';
import {useGetChartsData} from "../../Hooks/DashboardHooks";

const DashBoard = () => {
    const dispatch = useDispatch();
    const [chartsData, setChartsData] = useState({
        dateRange: {
            startDate: undefined,
            endDate: undefined
        },
        lineChartData: [],
        pieChartData: []
    });
    const [supabaseDataFilter, setSupabaseDataFilter] = useState({
        dataKey: "last_days",
        dataValue: "30"
    });
    useGetChartsData({
        setChartsData,
        supabaseDataFilter
    });
    console.log(chartsData)

    /*
    set breadcrumbs
    */

    useEffect(() => {
        const breadCrumbItems = {
            title: "Charts and graphs",
        };
        dispatch(setBreadcrumb(breadCrumbItems));
    }, [dispatch]);
    return (
        <React.Fragment>
             <MetaTags>
                <title>Dashboard | C-SCORE </title>
            </MetaTags>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col xxl={12}>
                            <Widgets chartsData={chartsData} setSupabaseDataFilter={setSupabaseDataFilter}/>
                            <UserActivity chartsData={chartsData} supabaseDataFilter={supabaseDataFilter} />
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default DashBoard;
