import React, { useState } from 'react';
import { Card, CardBody, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Row } from 'reactstrap';
import ChartArea from './ChartArea';

//import Components
import OrderState from './OrderState';
import TopProduct from './TopProduct';

const UserActivity = (props) => {
    //user Activity DropDown
    const [sortBy, setSortBy] = useState(false);
    const toggleSortBy = () => {
        setSortBy(!sortBy);
    };

    return (
        <React.Fragment>
            <Row>
                <Col xl={6}>
                    <Card>
                        <CardBody style={{height: 350}} className="pb-1">
                            <div className="d-flex align-items-start">
                                <div className="flex-grow-1">
                                    <h5 className="card-title mb-3">Traffic</h5>
                                </div>
                            </div>

                            <div className="m-n3">
                                <div id="chart-area"  style={{ minHeight : "285px"}}>
                                    <ChartArea chartsData={props.chartsData} />
                                </div>
                            </div>
                        </CardBody>
                    </Card>
                </Col>

                <Col xl={6} md={12}>
                    <OrderState chartsData={props.chartsData} />
                </Col>
            </Row>
        </React.Fragment>
    );
};

export default UserActivity;
