import React, { useState } from 'react';
import { Card, CardBody, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Row } from 'reactstrap';
import ChartDonut from './ChartDonut';

const OrderState = (props) => {
    //Order Stats DropDown
    const [orderStats, setOrderStats] = useState(false);
    const toggleOrderStats = () => {
        setOrderStats(!orderStats);
    };
    return (
        <React.Fragment>
            <Card>
                <CardBody style={{height: 350}}>
                    <div className="d-flex align-items-start">
                        <div className="flex-grow-1">
                            <h5 className="card-title mb-3">Top fraud geo</h5>
                        </div>
                    </div>

                    <div id="chart-donut" className="mt-2">
                        <ChartDonut chartsData={props.chartsData} supabaseDataFilter={props.supabaseDataFilter} />
                    </div>
                </CardBody>
            </Card>
        </React.Fragment>
    );
};

export default OrderState;