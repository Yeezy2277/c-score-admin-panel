import React from 'react';
import ReactApexChart from "react-apexcharts";

const ChartDonut = (props) => {
    const {chartsData} = props;
    const series = Object.values(chartsData.pieChartData);
    let options = {
        chart: {
            height: 350,
            type: 'donut',
            offsetY: 10
        },
        dataLabels: {
            enabled: false,
        },
        labels: Object.keys(chartsData.pieChartData),
        colors: ["#a7e79c", "#7c70de", "#f56e6e", "#4e9aea", "#e0de73", "#649d07", "#5073d3", "#de6597", "#cb6f09", "#393354"],
        fill: {
            type: 'gradient',
        },
        legend: {
            show: true,
            position: 'bottom',
            horizontalAlign: 'center',
            verticalAlign: 'middle',
            floating: false,
            fontSize: '14px',
            offsetX: 0,
        },
    };
    return (
        <React.Fragment>
            <ReactApexChart
                options={options}
                series={series}
                type="donut"
                height="261.8"
                className="apex-charts"
            />
        </React.Fragment>
    );
};

export default ChartDonut;