import React, {useEffect, useState} from 'react';
import {Card, CardBody, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Row} from 'reactstrap';
import {useGetChartsData} from "../../Hooks/DashboardHooks";

const Widgets = (props) => {
    const {setSupabaseDataFilter, chartsData} = props;
    const [isOpenedDropdown, setIsOpenedDropdown] = useState(false);
    const changeDropdownState = () => setIsOpenedDropdown(prevState => !prevState);
    const [dropdownItems, setDropdownItems] = useState(["Last 30 days", "This month", "Last month", "This quarter", "Last quarter"]);
    const [currentItem, setCurrentItem] = useState(dropdownItems[0]);
    useEffect(() => {
        if (currentItem === "Last 30 days") setSupabaseDataFilter({dataKey: "last_days", dataValue: "30"})
        else if (currentItem === "This month") setSupabaseDataFilter({dataKey: "month", dataValue: "this"})
        else if (currentItem === "Last month") setSupabaseDataFilter({dataKey: "month", dataValue: "last"})
        else if (currentItem === "Last quarter") setSupabaseDataFilter({dataKey: "quarter", dataValue: "last"})
        else if (currentItem === "This quarter") setSupabaseDataFilter({dataKey: "quarter", dataValue: "this"})
    }, [currentItem]);
    const [allTrafficCount, setAllTrafficCount] = useState(0);
    const [suspiciousCount, setSuspiciousCount] = useState(0);
    useEffect(() => {
        if (chartsData.lineChartData.length > 0) {
            setAllTrafficCount(chartsData.lineChartData.find(chart => chart.id === "Count registrations").data.map(item => item.y).reduce((a, b) => a + b))
            setSuspiciousCount(chartsData.lineChartData.find(chart => chart.id === "Count high fraud score registrations").data.map(item => item.y).reduce((a, b) => a + b))
        }
    }, [chartsData])

    const widgets = [
        {
            id: 1,
            type: "dropdown",
        },
        {
            id: 2,
            type: "common",
            icon: "people",
            title: "Count all traffic",
            count: String(allTrafficCount),
            badgeClass: "soft-danger",
            gainLoss: "100%"
        },
        {
            type: "common",
            id: 3,
            icon: "people",
            title: "Suspicious traffic",
            count: String(suspiciousCount),
            badgeClass: "soft-danger",
            gainLoss: `${suspiciousCount === 0 || allTrafficCount === 0 ? 0 : Math.floor(suspiciousCount / allTrafficCount)}%`
        },
        {
            type: "common",
            id: 4,
            icon: "people",
            title: "Profitable traffic",
            count: "340",
            badgeClass: "soft-danger",
            gainLoss: "21.6%"
        },
    ];
    return (
        <React.Fragment>
            <Row>
                {widgets.map((widgetsNew, key) => (
                    <Col xxl={3} xl={4} lg={5} key={key}>
                        <Card style={{height: 100}}>
                            <CardBody className={`card-body d-flex align-items-center w-100 ${widgetsNew.type === "dropdown" && "justify-content-center"}`}>
                                <div className="d-flex align-items-center justify-content-end ju">
                                    {widgetsNew.icon && <div className="flex-shrink-0 me-3">
                                        <div className="avatar">
                                            <div className="avatar-title rounded bg-primary bg-gradient">
                                                <i data-eva={widgetsNew.icon} width="24px" height="24px"
                                                   className="fill-white"></i>
                                            </div>
                                        </div>
                                    </div>}
                                    {widgetsNew.type === "dropdown" ? <div className="d-flex flex-grow-1 justify-content-center">
                                        <Dropdown isOpen={isOpenedDropdown} toggle={changeDropdownState}>
                                            <DropdownToggle
                                                type="button"
                                                tag="a"
                                                className="text-muted font-size-20"
                                            >
                                                {currentItem}<span><i className="mdi mdi-chevron-down ms-1"></i></span>
                                            </DropdownToggle>
                                            <DropdownMenu className="dropdown-menu-end">
                                                {dropdownItems.map(item => (
                                                    <DropdownItem onClick={() => setCurrentItem(item)} to="#">{item}</DropdownItem>
                                                ))}
                                            </DropdownMenu>
                                        </Dropdown>
                                    </div> : <div className="flex-grow-1">
                                        <p className="text-muted mb-1">{widgetsNew.title}</p>
                                        <h4 className="mb-0">{widgetsNew.count}</h4>
                                    </div>
                                    }

                                    <div className="flex-shrink-0 align-self-end ms-2">
                                        <div className={"badge rounded-pill font-size-13 badge-" + widgetsNew.badgeClass}>{widgetsNew.gainLoss}
                                        </div>
                                    </div>
                                </div>
                            </CardBody>
                        </Card>
                    </Col>
                ))}
            </Row>

        </React.Fragment>
    );
};

export default Widgets;
