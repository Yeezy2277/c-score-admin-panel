import React, { useEffect } from 'react';
import { Card, CardBody, Col, Container, Row } from 'reactstrap';
import MetaTags from "react-meta-tags";

// Import grid
import { Grid } from 'gridjs-react';

// Import Table Data
import { data, columns } from "./advanceTdata";

// redux
import { useDispatch } from "react-redux";
import { setBreadcrumb } from "../../store/actions";

const AdvanceTables = () => {
    const dispatch = useDispatch();

    /*
    set breadcrumbs
    */

    useEffect(() => {
        const breadCrumbItems = {
            title: "Advance Tables",
        };
        dispatch(setBreadcrumb(breadCrumbItems));
    }, [dispatch]);
    return (
        <React.Fragment>
            <MetaTags>
                <title>Advance Tables | C-SCORE </title>
            </MetaTags>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <div className="card-header">
                                    <h4 className="card-title mb-0">GridJs Table</h4>
                                </div>
                                <CardBody>
                                    <div id="table-gridjs">
                                        <Grid
                                            data={data}
                                            columns={columns}
                                            search={true}
                                            sort={true}
                                            pagination={{ enabled: true, limit: 5, }}
                                        />
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>

                    <Row>
                        <Col lg={12}>
                            <Card>
                                <div className="card-header">
                                    <h4 className="card-title mb-0">Pagination</h4>
                                </div>
                                <CardBody>
                                    <div id="table-pagination">
                                        <Grid
                                            data={data}
                                            columns={columns}
                                            pagination={{ enabled: true, limit: 5, }}
                                        />
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>

                    </Row>


                    <Row>
                        <Col lg={12}>
                            <Card>
                                <div className="card-header">
                                    <h4 className="card-title mb-0">Search</h4>
                                </div>
                                <CardBody>
                                    <div id="table-search">
                                        <Grid
                                            data={data}
                                            columns={columns}
                                            search={true}
                                            pagination={{ enabled: true, limit: 5, }}
                                        />
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>

                    </Row>


                    <Row>
                        <Col lg={12}>
                            <Card>
                                <div className="card-header">
                                    <h4 className="card-title mb-0">Sorting</h4>
                                </div>
                                <CardBody>
                                    <div id="table-sorting">
                                        <Grid
                                            data={data}
                                            columns={columns}
                                            sort={true}
                                            pagination={{ enabled: true, limit: 5, }}
                                        />
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>

                    </Row>


                    <Row>
                        <Col lg={12}>
                            <Card>
                                <div className="card-header">
                                    <h4 className="card-title mb-0">Loading State</h4>
                                </div>
                                <CardBody>
                                    <div id="table-loading-state">
                                        <Grid
                                            data={function () {
                                                return new Promise(function (resolve) {
                                                    setTimeout(function () {
                                                        resolve(data)
                                                    }, 2000);
                                                });
                                            }}
                                            columns={columns}
                                            sort={true}
                                            pagination={{ enabled: true, limit: 5, }}
                                        />
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>

                    </Row>


                    <Row>
                        <Col lg={12}>
                            <Card>
                                <div className="card-header justify-content-between d-flex align-items-center">
                                    <h4 className="card-title mb-0">Fixed Header</h4>
                                </div>
                                <CardBody>
                                    <div id="table-fixed-header">
                                        <Grid
                                            data={data}
                                            columns={columns}
                                            search={true}
                                            sort={true}
                                            fixedHeader={true}
                                            height={'400px'}
                                            pagination={{ enabled: true, limit: 5 }}
                                        />
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>

                    </Row>


                    <Row>
                        <Col lg={12}>
                            <Card>
                                <div className="card-header">
                                    <h4 className="card-title mb-0">Hidden Columns</h4>
                                </div>
                                <CardBody>
                                    <div id="table-hidden-column">
                                        <Grid
                                            data={data}
                                            columns={["Name", "Email", "Position", "Company", { name: 'Country', hidden: true }]}
                                            sort={true}
                                            pagination={{ enabled: true, limit: 5, }}
                                        />
                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default AdvanceTables;