import React, { useEffect } from 'react';
import { Card, CardBody, CardHeader, Col, Container, Row } from 'reactstrap';
import MetaTags from "react-meta-tags";

// redux
import { useDispatch } from "react-redux";
import { setBreadcrumb } from "../../store/actions";

// Form Mask
import InputMask from "react-input-mask";

const Mask = () => {
    const dispatch = useDispatch();

    /*
    set breadcrumbs
    */

    useEffect(() => {
        const breadCrumbItems = {
            title: "Form Mask",
        };
        dispatch(setBreadcrumb(breadCrumbItems));
    }, [dispatch]);

    const DateStyle = (props) => (
        <InputMask
            mask="99.99.9999"
            value={props.value}
            className="form-control input-color"
            onChange={props.onChange}
        >
        </InputMask>
    );

    const Number = (props) => (
        <InputMask
            mask="+7(999)-999-99-99"
            value={props.value}
            className="form-control input-color"
            onChange={props.onChange}
        >
        </InputMask>
    );

    const NumberOrEmail = (props) => (
        <InputMask
            mask="+7(999)-999-99-99 Or _@_._"
            value={props.value}
            className="form-control input-color"
            onChange={props.onChange}
        >
        </InputMask>
    );


    const RangNumber = (props) => (
        <InputMask
            mask="99999"
            value={props.value}
            className="form-control input-color"
            onChange={props.onChange}
        >
        </InputMask>
    );

    const RegExp = props => (
        <InputMask
            mask="99999*****"
            value={props.value}
            className="form-control input-color"
            onChange={props.onChange}
        >
        </InputMask>
    );

    const Currency = props => (
        <InputMask
            mask="$ 999,999,999.99"
            value={props.value}
            className="form-control input-color"
            onChange={props.onChange}
        >
        </InputMask>
    );

    return (
        <React.Fragment>
            <MetaTags>
                <title>Form Mask | C-SCORE </title>
            </MetaTags>
            <div className="page-content">
                <Container fluid>
                    <Row>
                        <Col lg={12}>
                            <Card>
                                <CardHeader>
                                    <h4 className="card-title">Imask</h4>
                                </CardHeader>
                                <CardBody>
                                    <p className="card-title-desc">vanilla javascript input mask</p>
                                    <form>
                                        <Row>
                                            <Col lg={6}>
                                                <div>
                                                    <div>
                                                        <label htmlFor="regexp-mask" className="form-label">RegExp (Russian postal code)</label>
                                                        <RegExp id="regexp-mask" />
                                                        <div className="text-muted">/^[1-6]\d 0,5 $/</div>
                                                    </div>

                                                    <div className="mt-4">
                                                        <label htmlFor="phone-mask" className="form-label">Pattern (Phone)</label>
                                                        <Number id="phone-mask" />
                                                        <div className="text-muted">+{7}(000)000-00-00</div>
                                                    </div>
                                                    <div className="mt-4">
                                                        <label htmlFor="number-mask" className="form-label">Number</label>
                                                        <RangNumber id="number-mask" />
                                                        <div className="text-muted">in range [-10000, 10000]</div>
                                                    </div>
                                                </div>
                                            </Col>

                                            <Col lg={6}>
                                                <div className="mt-4 mt-lg-0">
                                                    <div>
                                                        <label htmlFor="date-mask" className="form-label">Date</label>
                                                        <DateStyle id="date-mask" />
                                                        <div className="text-muted">'dd.mm.yyyy' in range [01.01.1990, 01.01.2020]</div>
                                                    </div>

                                                    <div className="mt-4">
                                                        <label className="form-label">On-the-fly select</label>
                                                        <NumberOrEmail id="dynamic-mask" />
                                                        <div className="text-muted">phone or email</div>
                                                    </div>
                                                    <div className="mt-4">
                                                        <label className="form-label">Mask in mask</label>
                                                        <Currency id="currency-mask" />
                                                        <div className="text-muted">currency input</div>
                                                    </div>
                                                </div>
                                            </Col>
                                        </Row>
                                    </form>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        </React.Fragment>
    );
};

export default Mask;