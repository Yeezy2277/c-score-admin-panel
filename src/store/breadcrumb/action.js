import { SET_BREADCRUMB } from "./actionType";

// common success

export const setBreadcrumb = (breadCrumbItems) => ({
  type: SET_BREADCRUMB,
  payload: breadCrumbItems,
});
