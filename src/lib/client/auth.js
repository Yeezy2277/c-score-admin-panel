import { useMemo } from 'react';
import { useNavigate, useLocation } from 'react-router-dom';
export function useAuthRedirectUrl(path = "/") {
    const navigate = useNavigate();
    const location = useLocation();

    const { locale, defaultLocale } = navigate; // Assuming locale and defaultLocale are part of your navigate object

    const baseUrl = useMemo(() => (typeof window !== 'undefined' ? window.location.origin : '/'), []);
    const localePath = useMemo(() => (locale === defaultLocale ? '' : `/${locale}`), [locale, defaultLocale]);
    const formattedPath = useMemo(() => (path.startsWith('/') ? path : `/${path}`), [path]);

    return useMemo(() => `${baseUrl}${localePath}${formattedPath}`, [localePath, formattedPath, baseUrl]);
}
