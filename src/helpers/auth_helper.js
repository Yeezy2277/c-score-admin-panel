import {useContext} from "react";
import {RouterCacheContext} from "../routes";

export const useRouterCache = () => {
    const { cache, setCache } = useContext(RouterCacheContext);

    const invalidateCache = () => {
        setCache({});
    };

    return {
        cache,
        invalidateCache,
    };
};