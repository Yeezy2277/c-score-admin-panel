export const getAggregatedData = ({startDate, endDate, oneDayInMilliseconds, tempData}) => {

    const aggregatedData = {};
    const upperLimit = endDate?.getTime();

    let i = startDate?.getTime() + oneDayInMilliseconds, k = 1;

    for (i; i <= upperLimit + oneDayInMilliseconds;) {

        const shortDate = new Date(i).toISOString().split("T")[0];
        const objKeys = Object.keys(tempData);

        if (objKeys.includes(shortDate)) {

            Object.assign(aggregatedData, {
                [shortDate]: {
                    suspicious: {
                        countGamblers: 0,
                    },
                    combined: {
                        countGamblers: 0
                    }
                }
            });

            Object.assign(aggregatedData, {
                [shortDate]: ["suspicious", "notSuspicious"].reduce(
                    (accumulator, currentValue) => {
                        if (currentValue === "suspicious") {
                            return Object?.assign(accumulator, {
                                suspicious: {
                                    countGamblers: tempData[shortDate].reduce((accumulator, currentValue) => (
                                        currentValue.suspicious ? accumulator + currentValue.count_gamblers : accumulator
                                    ), 0)
                                }
                            });
                        } else if (currentValue === "notSuspicious") {
                            return Object?.assign(accumulator, {
                                combined: {
                                    countGamblers: tempData[shortDate].reduce((accumulator, currentValue) => (
                                        accumulator + currentValue.count_gamblers
                                    ), 0)
                                }
                            });
                        }
                        // @ts-ignore
                    }, { ...aggregatedData[shortDate] }
                )
            });
        } else {
            Object.assign(aggregatedData, {
                [shortDate]: {
                    suspicious: {
                        countGamblers: 0,
                    },
                    combined: {
                        countGamblers: 0
                    }
                }
            });
        }

        i += oneDayInMilliseconds;
        k++;
    }

    return aggregatedData;
};

export const getDashboardsFinalData = (aggregatedData) => {

    return Object.keys(aggregatedData).reduce((accum, date) => {
        return [
            {
                "id": "Count registrations",
                "color": "#ee204d",
                "data": [
                    ...accum[0].data,
                    {
                        x: date,
                        // @ts-ignore
                        y: aggregatedData[date].suspicious.countGamblers
                    }
                ]
            },
            {
                "id": "Count high fraud score registrations",
                "color": "#c5c6fa",
                "data": [
                    ...accum[1].data,
                    {
                        x: date,
                        // @ts-ignore
                        y: aggregatedData[date].combined.countGamblers
                    }
                ]
            }
        ];
    }, [
        {
            "id": "suspicious",
            "color": "#ee204d",
            "data": []
        },
        {
            "id": "combined",
            "color": "#c5c6fa",
            "data": []
        }
    ]);
};

export const defineDatePeriod = ({ supabaseDataFilter, currentYear, currentMonth, currentDate, oneDayInMilliseconds }) => {
    let startDate, endDate;
    if (supabaseDataFilter.dataKey === "month" && supabaseDataFilter.dataValue === "this") {

        startDate = new Date(currentYear, currentMonth, 1);
        endDate = new Date(currentYear, currentMonth + 1, 0);

    } else if (supabaseDataFilter.dataKey === "month" && supabaseDataFilter.dataValue === "last") {

        endDate = new Date(new Date(currentYear, currentMonth, 0).getTime());
        startDate = new Date(endDate.getTime() - ((endDate.getDate() - 1) * oneDayInMilliseconds));

    } else if (supabaseDataFilter.dataKey === "last_days" && supabaseDataFilter.dataValue === "30") {

        endDate = new Date(new Date(new Date(currentDate).getTime() - oneDayInMilliseconds).setHours(0, 0, 0, 0));
        startDate = new Date(endDate.getTime() - (29 * oneDayInMilliseconds));

    } else if (supabaseDataFilter.dataKey === "quarter" && supabaseDataFilter.dataValue === "this") {

        endDate = new Date(new Date(new Date(currentDate).getTime() - oneDayInMilliseconds).setHours(0, 0, 0, 0));
        startDate = new Date(endDate.getTime() - (89 * oneDayInMilliseconds));

    } else if (supabaseDataFilter.dataKey === "quarter" && supabaseDataFilter.dataValue === "last") {

        endDate = new Date(new Date(new Date(currentDate).getTime() - (oneDayInMilliseconds * 90)).setHours(0, 0, 0, 0));
        startDate = new Date(endDate.getTime() - (89 * oneDayInMilliseconds));

    }

    return {
        startDate,
        endDate
    };
};

export const changeDashboardPeriod = ({ supabaseDataFilter, setSupabaseDataFilter}) => (e) => {

    const selectValue = e.currentTarget.value;
    let filter = {
        dataKey: supabaseDataFilter.dataKey,
        dataValue: supabaseDataFilter.dataValue
    };

    if (selectValue === "last_30_days") {
        filter = {
            dataKey: "last_days",
            dataValue: "30"
        };
    } else if (selectValue === "this_month") {
        filter = {
            dataKey: "month",
            dataValue: "this"
        };
    } else if (selectValue === "last_month") {
        filter = {
            dataKey: "month",
            dataValue: "last"
        };
    } else if (selectValue === "this_quarter") {
        filter = {
            dataKey: "quarter",
            dataValue: "this"
        };
    }

    setSupabaseDataFilter(filter);
};
